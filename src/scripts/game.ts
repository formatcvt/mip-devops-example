import 'phaser';

import calculator from "./Calculator";

const gameOptions = {

    // maximum length of the sum
    maxSumLen: 5,

    // local storage name used to save high score
    localStorageName: "oneplustwo",

    // time allowed to answer a question, in milliseconds
    timeToAnswer: 3000,

    // score needed to increase difficulty
    nextLevel: 400,
    timerTotalSteps: 100,
}
export default class Demo extends Phaser.Scene {
    isGameOver: boolean = false;
    score: number = 0;
    correctAnswers: number = 0;
    topScore: number = 0;
    sumsArray: string[][][] = [];
    questionText: Phaser.GameObjects.Text;
    scoreText: Phaser.GameObjects.Text;
    button1: Phaser.GameObjects.Sprite;
    button2: Phaser.GameObjects.Sprite;
    button3: Phaser.GameObjects.Sprite;
    timeTween: Phaser.Tweens.Tween;
    //buttonMask: Phaser.GameObjects.Graphics;
    numberTimer: Phaser.GameObjects.Sprite;
    randomSum: number;
    cam: Phaser.Cameras.Scene2D.Camera;
    timer: Phaser.Time.TimerEvent;
    cropRect: Phaser.Geom.Rectangle;

    constructor() {
        super('demo');
    }

    preload() {
        this.load.image('timebar', 'assets/timebar.png');
        this.load.spritesheet('buttons', 'assets/buttons.png', {
            frameWidth: 400,
            frameHeight: 50
        });
    }

    create() {
        this.cam = this.cameras.add(0, 0, config.width, config.height);
        this.cam.setBackgroundColor(0xcccccc);

        // it's not game over yet...
        this.isGameOver = false;

        // current score is set to zero
        this.score = 0;

        // we'll also keep track of correct answers
        this.correctAnswers = 0;

        // topScore gets the previously saved value in local storage if any, zero otherwise
        this.topScore = localStorage.getItem(gameOptions.localStorageName) == null ? 0 : Number(localStorage.getItem(gameOptions.localStorageName));

        // sumsArray is the array with all possible questions
        this.sumsArray = [];

        // rather than tossing a random question each time, I found easier
        // to store all possible questions in an array then draw a random question
        // each time. I just need an algorithm to generate all possible questions.

        // let's start building all possible questions with this loop
        // ranging from 1 (only one operator, like 1+1) to maxSumLen
        // (in this case 5, like 1+1+1+1-1-1)
        for (var i = 1; i < gameOptions.maxSumLen; i++) {

            // defining sumsArray[i] as an array of three empty arrays
            this.sumsArray[i] = [[], [], []];

            // looping from 1 to 3, which are the possible results of each sum
            for (var j = 1; j <= 3; j++) {

                // buildTrees is the core of the script, see it explained
                // some lines below
                this.buildThrees(j, 1, i, j.toString());
            }
        }

        // try this! You will see all possible combinations
        console.log(this.sumsArray);

        // questionText is the text object which will display the question
        this.questionText = this.add.text(250, 160, "-", {
            font: "bold 72px Arial"
        });
        this.questionText.setAlign('center');

        // setting questionText registration point
        this.questionText.setOrigin(0.5, 0.5);

        // scoreText will keep the current score
        this.scoreText = this.add.text(10, 10, "-", {
            font: "bold 24px Arial"
        });

        // loop to create the three answer buttons
        this.button1 = this.add.sprite(50, 250 + 0 * 75, "buttons", "0").setInteractive();
        this.button1.input.cursor = "pointer";
        this.button1.setOrigin(0, 0.5);
        var that = this;
        this.button1.on('pointerdown', function (event) {
            that.checkAnswer(0);
        });
        this.button2 = this.add.sprite(50, 250 + 1 * 75, "buttons", "1").setInteractive();
        this.button2.input.cursor = "pointer";
        this.button2.setOrigin(0, 0.5);
        this.button2.on('pointerdown', function (event) {
            that.checkAnswer(1);
        });
        this.button3 = this.add.sprite(50, 250 + 2 * 75, "buttons", "2").setInteractive();
        this.button3.input.cursor = "pointer";
        this.button3.setOrigin(0, 0.5);
        this.button3.on('pointerdown', function (event) {
            that.checkAnswer(2);
        });
        // adding the time bar
        this.numberTimer = this.add.sprite(50, 250 + 1 * 75, "timebar");
        this.numberTimer.setOrigin(0, 0.5);
        this.cropRect = new Phaser.Geom.Rectangle(0, 0, this.numberTimer.width, this.numberTimer.height);
        this.nextNumber();
    }
    update() {
        this.numberTimer.setCrop(this.cropRect);
    }
    // buildThrees method, it will find all possible sums
    // arguments:
    // initialNumber: the first number. Each question always start with a positive number
    // currentIndex: it's the amount of operands already placed in the sum
    // limit: the max amount of operands allowed in the question
    // currentString: the string generated so far
    buildThrees(initialNummber: number, currentIndex: number, limit: number, currentString: String) {

        // the possible operands, from -3 to 3, excluding the zero
        var numbersArray = [-3, -2, -1, 1, 2, 3];

        // looping from 0 to numbersArray's length
        for (var i = 0; i < numbersArray.length; i++) {

            // "sum" is the sum between the first number and current numberArray item
            var sum = calculator.Sum(initialNummber, numbersArray[i]);

            // output string is generated by the concatenation of current string with
            // current numbersArray item. I am adding a "+" if the item is greater than zero,
            // otherwise it already has its "-"
            var outputString = currentString + (numbersArray[i] < 0 ? "" : "+") + numbersArray[i];

            // if sum is between 1 and 3 and we reached the limit of operands we want...
            if (sum > 0 && sum < 4 && currentIndex == limit) {

                // then push the output string into sumsArray[amount of operands][result]
                this.sumsArray[limit][sum - 1].push(outputString);
            }

            // if the amount of operands is still below the amount we want...
            if (currentIndex < limit) {

                // recursively calling buildThrees, passing as arguments:
                // the current sum
                // the new amount of operands
                // the amount of operands we want
                // the current output string
                this.buildThrees(sum, currentIndex + 1, limit, outputString);
            }
        }
    }

    // this method asks next question
    nextNumber() {

        // updating score text
        this.scoreText.text = "Score: " + this.score.toString() + "\nBest Score: " + this.topScore.toString();

        // if we already answered more than one question...
        if (this.correctAnswers > 0) {

            // stopping time tween
            //this.cropRect = new Phaser.Geom.Rectangle(0, 0, this.numberTimer.width, this.numberTimer.height);
            if (this.timeTween != undefined) {
                this.timeTween.stop();
                this.timeTween.restart();
                this.timeTween.remove();
            }
            this.timeTween = this.tweens.add({
                targets: this.cropRect,
                width: 0,
                duration: gameOptions.timeToAnswer,
                // callback to be triggered when the tween ends
                onComplete: () => {
                    // calling "gameOver" method. "?" is the string to display
                    this.gameOver("?")
                },
                //repeat: -1,
                //paused: true,
            });

            // resetting mask horizontal position
            //this.buttonMask.x = 50;
        }
        // drawing a random result between 0 and 2 (it will be from 1 to 3)
        this.randomSum = Phaser.Math.Between(0, 2);

        // choosing question length according to current score
        var questionLength = Math.min(Math.floor(this.score / gameOptions.nextLevel) + 1, 4)

        // updating question text
        this.questionText.setText(this.sumsArray[questionLength][this.randomSum][Phaser.Math.Between(0, this.sumsArray[questionLength][this.randomSum].length - 1)]);
    }

    // method to check the answer, the argument is the button pressed
    checkAnswer(index: number) {

        // we check the answer only if it's not game over yet
        if (!this.isGameOver) {
            //alert(`index: ${index} this.randomSum: ${this.randomSum}`);

            // button frame is equal to randomSum means the answer is correct
            if (index == this.randomSum) {
                // score is increased according to the time spent to answer
                this.score += Math.floor((this.cropRect.width) / 4);

                // one more correct answer
                this.correctAnswers++;

                // moving on to next question
                this.nextNumber();
            }

            // wrong answer
            else {

                // if it's not the first question...
                if (this.timeTween != undefined) {
                    this.timeTween.stop();
                    this.timeTween.restart();
                    this.timeTween.remove();
                }

                // calling "gameOver" method. "button.frame + 1" is the string to display
                this.gameOver((index + 1).toString());
            }
        }
    }

    // method to end the game. The argument is the string to write
    gameOver(gameOverString: string) {

        // changing background color
        //this.stage.backgroundColor = "#ff0000";
        this.cam.setBackgroundColor(0xff0000);

        // displaying game over text
        this.questionText.setText(`${this.questionText.text} = ${gameOverString}`);

        // now it's game over
        this.isGameOver = true;

        // updating top score in local storage
        localStorage.setItem(gameOptions.localStorageName, Math.max(this.score, this.topScore).toString());

        // restart the game after two seconds
        this.time.addEvent({
            delay: gameOptions.timeToAnswer,
            callback: () => {
                this.scene.restart()
            }
        })
    }
}

const config = {
    type: Phaser.AUTO,
    backgroundColor: '#125555',
    width: 500,
    height: 500,
    parent: 'phaser-game',
    scale: {
        mode: Phaser.Scale.FIT,
        autoCenter: Phaser.Scale.CENTER_BOTH
    },
    scene: Demo
};

const game = new Phaser.Game(config);
